<?php
  require_once 'vendor/autoload.php';
   
    /*  DATOS DE LA ACTIVIDAD */
    include  'conexion.php';
    
    // Conexión
    $cn    = ConexionMySql();

    // Selección de actividad por parámetro
    $sql   = "SELECT * FROM actividades WHERE idActividad=:idActividad";
    $idActividad = intval($_GET["idActividad"]);
    $query = $cn->prepare($sql);
    $query->bindParam('idActividad', $idActividad);
    $query->execute();
    $r = $query->fetch(PDO::FETCH_ASSOC);
    
    // Selección responsables
    $sqlResponsable   = "SELECT nombre FROM responsables WHERE idResponsable = :idResponsable";
    $queryResponsable = $cn->prepare($sqlResponsable);
    
    // Selección aula
    $sqlAula = "SELECT nombre FROM aulas WHERE idAula=:aulaId";
    $queryAula = $cn->prepare($sqlAula);
    $queryAula->bindParam(':aulaId', $r['aulaId']);
    $queryAula->execute();
    $aula =$queryAula->fetch(PDO::FETCH_ASSOC);
    
    // Selección grupo
    $sqlGrupo = "SELECT nombre FROM grupos WHERE idGrupo=:grupoId";
    $queryGrupo = $cn->prepare($sqlGrupo);
    $queryGrupo->bindParam(':grupoId', $r['grupoId']);
    $queryGrupo->execute();
    $grupo =$queryGrupo->fetch(PDO::FETCH_ASSOC);
    
    // Selección categoias
    $sqlCategoria   = "SELECT nombre FROM categorias WHERE idCategoria=:categoriaId";
    $queryCategoria = $cn->prepare($sqlCategoria);
    $queryCategoria->bindParam(':categoriaId', $r['categoriaId']);
    $queryCategoria->execute();
    $categoria =$queryCategoria->fetch(PDO::FETCH_ASSOC);
    
    
    // Selección responsables dependiendo de la actividad
    $sqlActividadResponsable   = "SELECT * FROM actividadesResponsables WHERE idActividadResponsable = :actividadResponsableId";
    $queryActividadResponsable = $cn->prepare($sqlActividadResponsable);
    $queryActividadResponsable->bindParam(':actividadResponsableId', $r['actividadResponsableId']);
    $queryActividadResponsable->execute();

    // Cálculo del tiempo de la actividad
    $sqlTiempo = 'SELECT timediff(horaFin, horaInicio) HORAS FROM actividades WHERE idActividad=:id';
    $queryTiempo = $cn->prepare($sqlTiempo);
    $queryTiempo->bindParam(':id',$r['idActividad']);
    $queryTiempo->execute();
    $tiempo = $queryTiempo->fetch(PDO::FETCH_ASSOC);
    
    $encontrado = false;
    $contador = 0;
    $nombres = array();
    while ($s = $queryActividadResponsable->fetch(PDO::FETCH_ASSOC)) {
        $queryResponsable->bindParam('idResponsable',$s['responsableId']);
        $queryResponsable->execute();
        $j = $queryResponsable->fetch(PDO::FETCH_ASSOC);
        array_push($nombres,$j['nombre']);
    }
    // Template processor instance creation


//Set header to show as PDF
//header("Content-Type: application/pdf");
//header("Content-Disposition: attachment; filename=" .$r['nombre'].'.pdf');

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Concejalia de Juventud');
$pdf->SetTitle($r['nombre']);
$pdf->SetSubject('Resumen actividad');
$pdf->SetKeywords('Actividades, horarios, grupos');

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, 30, PDF_MARGIN_RIGHT);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('times', '', 12);

// add a page
$pdf->AddPage();

/*
    Cell ($ancho, $alto=0, $texto="", $borde=0, $salto_de_linea=0, 
    $alineacion="", $fondo=false, $enlace="", $ajuste_horizontal=0, 
    $ignore_min_height=false, $alin_vertical_texto='T', 
    $alineacion_vertical='M')
*/

/* 
    Multicell ($ancho, $alto, $texto, $borde=0, $alineacion='J', 
    $fondo=false, $salto_de_linea=1, $x='', $y='', $reseth=true, 
    $ajuste_horizontal=0, $ishtml=false, $autopadding=true, $maxh=0, 
    $alineacion_vertical='T', $fitcell=false)) 
*/

$pdf->SetFont ( 'times','B',20);
$pdf->Cell(0, 0, "FICHA DE: ".'"' . strtoupper(sprintf("%s", $r["nombre"])) . '"', 0, 1, 'C');
$pdf->Ln();

$pdf->SetFont ( 'times','B',12);
$pdf->Cell(70, 0, "ACTIVIDAD: ", 0, 0, 'L');
$pdf->SetFont ( 'times','',12);
$pdf->Cell(0, 0,sprintf("%s", $r["nombre"]),0, 0, 'L');
$pdf->Ln();
$pdf->SetFont ( 'times','B',12);
$pdf->Cell(70, 0, "GRUPO: ", 0, 0, 'L');
$pdf->SetFont ( 'times','',12);
$pdf->Cell(0, 0,sprintf("%s", $grupo['nombre']),0, 0, 'L');
$pdf->Ln();
$pdf->SetFont ( 'times','B',12);
$pdf->Cell(70, 0, "CATEGORIA: ", 0, 0, 'L');
$pdf->SetFont ( 'times','',12);
$pdf->Cell(0, 0,sprintf("%s", $categoria['nombre']) ,0, 0, 'L');
$pdf->Ln();
$pdf->SetFont ( 'times','B',12);
$pdf->Cell(70, 0, "DIAS: ", 0, 0, 'L');
$pdf->SetFont ( 'times','',12);
$pdf->Cell(0, 0,sprintf("%s", $r['dias']) ,0, 0, 'L');
$pdf->Ln();
$pdf->SetFont ( 'times','B',12);
$pdf->Cell(70, 0, "PARTICIPANTES MÍNIMOS: ", 0, 0, 'L');
$pdf->SetFont ( 'times','',12);
$pdf->Cell(0, 0,sprintf("%s", $r['parMin']) ,0, 0, 'L');
$pdf->Ln();
$pdf->SetFont ( 'times','B',12);
$pdf->Cell(70, 0, "PARTICIPANTES MÁXIMOS: ", 0, 0, 'L');
$pdf->SetFont ( 'times','',12);
$pdf->Cell(0, 0,sprintf("%s", $r['parMax']) ,0, 0, 'L');
$pdf->Ln();
$pdf->SetFont ( 'times','B',12);
$pdf->Cell(70, 0, "EDAD MÍNIMA: ", 0, 0, 'L');
$pdf->SetFont ( 'times','',12);
$pdf->Cell(0, 0,sprintf("%s", $r['edadMinima']) ,0, 0, 'L');
$pdf->Ln();
$pdf->SetFont ( 'times','B',12);
$pdf->Cell(70, 0, "EDAD MAXIMA: ", 0, 0, 'L');
$pdf->SetFont ( 'times','',12);
$pdf->Cell(0, 0,sprintf("%s", $r['edadMaxima']) ,0, 0, 'L');
$pdf->Ln();
$pdf->SetFont ( 'times','B',12);
$pdf->Cell(70, 0, "FECHA DE INSCRIPCIONES: ", 0, 0, 'L');
$pdf->SetFont ( 'times','',12);
$pdf->Cell(0, 0,sprintf("%s", $r['fechaInscripciones']) ,0, 0, 'L');
$pdf->Ln();
$pdf->SetFont ( 'times','B',12);
$pdf->Cell(70, 0, "FECHA DE INICIO: ", 0, 0, 'L');
$pdf->SetFont ( 'times','',12);
$pdf->Cell(0, 0,sprintf("%s", $r['fechaInicio']) ,0, 0, 'L');
$pdf->Ln();
$pdf->SetFont ( 'times','B',12);
$pdf->Cell(70, 0, "FECHA DE FINALIZACIÓN: ", 0, 0, 'L');
$pdf->SetFont ( 'times','',12);
$pdf->Cell(0, 0,sprintf("%s", $r['fechaFin']) ,0, 0, 'L');
$pdf->Ln();
$pdf->SetFont ( 'times','B',12);
$pdf->Cell(70, 0, "HORA DE INICIO: ", 0, 0, 'L');
$pdf->SetFont ( 'times','',12);
$pdf->Cell(0, 0,sprintf("%s", $r['horaInicio']) ,0, 0, 'L');
$pdf->Ln();
$pdf->SetFont ( 'times','B',12);
$pdf->Cell(70, 0, "HORA DE FINALIZACIÓN: ", 0, 0, 'L');
$pdf->SetFont ( 'times','',12);
$pdf->Cell(0, 0,sprintf("%s", $r['horaFin']) ,0, 0, 'L');
$pdf->Ln();
$pdf->SetFont ( 'times','B',12);
$pdf->Cell(70, 0, "DURACIÓN: ", 0, 0, 'L');
$pdf->SetFont ( 'times','',12);
$pdf->Cell(0, 0,sprintf("%s", $tiempo['HORAS']) ,0, 0, 'L');
$pdf->Ln();
$pdf->SetFont ( 'times','B',12);
$pdf->Cell(70, 0, "PRECIO: ", 0, 0, 'L');
$pdf->SetFont ( 'times','',12);
$pdf->Cell(0, 0,sprintf("%s", $r['precio'])." €" ,0, 0, 'L');

$pdf->Ln();
$pdf->SetFont ( 'times','B',12);
$pdf->Cell(70, 0, "RESPONSABLE(S): ", 0, 0, 'L');
$pdf->SetFont ( 'times','',12);
$pdf->Cell(0, 0,sprintf("%s", implode(",", $nombres) ) ,0, 0, 'L');

$pdf->Ln();
$pdf->SetFont ( 'times','B',12);
$pdf->Cell(70, 0, "UBICACIÓN: ", 0, 1, 'L');
$pdf->SetFont ( 'times','',12);
$pdf->Cell(0, 0,sprintf("%s",$r['lugarCelebracion']) ,0, 0, 'L');
$pdf->Ln();
$pdf->SetFont ( 'times','B',12);
$pdf->Cell(70, 0, "INCLUYE: ", 0, 1, 'L');
$pdf->SetFont ( 'times','',12);
$pdf->MultiCell(0, 0,sprintf("%s",$r['incluye']) ,0,'L');
$pdf->SetFont ( 'times','B',12);
$pdf->Cell(70, 0, "TEXTO PREVIO PARA FOLLETO: ", 0, 1, 'L');
$pdf->SetFont ( 'times','',12);
$pdf->MultiCell(0, 0,sprintf("%s",$r['textoPrevioFolleto']) ,0,'L');
$pdf->SetFont ( 'times','B',12);
$pdf->MultiCell(0, 0, "OBSERVACIONES: ", 0,'L');
$pdf->SetFont ( 'times','',12);
$pdf->Cell(0, 0,sprintf("%s",$r['observaciones']) ,0, 0, 'L');

//Close and output PDF document
$pdf->Output($r['nombre'].'.pdf', 'D');

//============================================================+
// END OF FILE
//============================================================+

?>