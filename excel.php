<?php

error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/Madrid');

if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');

include  'conexion.php';
$cn    = ConexionMySql();


$consulta = "SELECT * FROM actividades WHERE categoriaId=:id";
$queryConsulta = $cn->prepare($consulta);
//$resultado = $cn->query($consulta);

// Diferencia de tiempos SQL SELECT timediff(horaFin, horaInicio) HORAS FROM actividades;
$sqlTiempo = 'SELECT timediff(horaFin, horaInicio) HORAS FROM actividades WHERE idActividad=:id';
$queryTiempo = $cn->prepare($sqlTiempo);

// Preparamos la consulta para sacar el grupo
$sqlGrupo = 'SELECT nombre FROM grupos WHERE idGrupo = :grupoId';
$queryGrupo = $cn->prepare($sqlGrupo);
// Preparamos la consulta para sacar el aula
$sqlAula = 'SELECT nombre FROM aulas WHERE idAula = :aulaId';
$queryAula = $cn->prepare($sqlAula);
// Preparamos la consulta para sacar los responsables de cada actividad
$sqlResponsable = 'SELECT nombre FROM responsables WHERE idResponsable = :responsableId';
$queryResponsable = $cn->prepare($sqlResponsable);
// Preparamos la consulta para sacar la categoria
$sqlCategoria = 'SELECT nombre FROM categorias WHERE idCategoria = :categoriaId';
$queryCategoria = $cn->prepare($sqlCategoria);

// Preparamos la consulta para sacar la categoria
$sqlCategoria2 = 'SELECT idCategoria FROM categorias';
$queryCategoria2 = $cn->prepare($sqlCategoria2);
$queryCategoria2->execute();

// Preparamos la consulta para sacar la actividadresponsable
$sqlActividadResponsable = 'SELECT responsableId FROM actividadesresponsables WHERE idActividadResponsable = :actividadResponsableId';
$queryActividadResponsable = $cn->prepare($sqlActividadResponsable);

//var_dump($resultado);
//var_dump($resultado->fetch());
/** Include PHPExcel */
//require_once dirname(__FILE__) . 'Classes\PHPExcel.php';
require_once 'Classes\PHPExcel.php';

$titulosColumnas = array('ACTIVIDAD','GRUPO','RESPONSABLE','FECHA INICIO','FECHA FIN',
'DIAS','AULA','HORARIO INICIO','HORARIO FIN', 'HORAS', 'PRECIO', 'PAR. MIN', 'PAR. MAX',	
'EDAD MIN',	'EDAD MAX',	'FECHA INSCRIPCIONES', 'INCLUYE', 'TEXTO PREVIO FOLLETO', 'LUGAR DE CELEBRACIÓN', 'OBSERVACIONES');


// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Definimos estilos
$estiloTituloColumnas = array(
    'font' => array(
        'name'  => 'Arial',
        'bold'  => true,
        'color' => array(
            'rgb' => '000000'
        )
    ),
    'alignment' =>  array(
        'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        'wrap'      => TRUE
    ),
    'fill' => array(
        'type'       => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
  'rotation'   => 90,
        'startcolor' => array(
            'rgb' => 'b2b8ce'
        ),
        'endcolor' => array(
            'argb' => 'b2b8ce'
        )
    )
);

$estiloTotales = array(
    'font' => array(
        'name'  => 'Calibri',
        'bold'  => false,
        'color' => array(
            'rgb' => '000000'
        )
    ),
    'alignment' =>  array(
        'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        'wrap'      => TRUE
    ),
    'fill' => array(
        'type'       => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
  'rotation'   => 90,
        'startcolor' => array(
            'rgb' => 'c3c4c9'
        ),
        'endcolor' => array(
            'argb' => 'c3c4c9'
        )
    )
);

$estiloCategorias = array();

$estiloCategorias[1] = array(
    'font' => array(
        'name'  => 'Antique Olive Compact',
        'bold'  => true,
        'size' =>20,
        'color' => array(
            'rgb' => '1F0F10'
        )
        ),
        'fill' => array(
            'type'       => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
      'rotation'   => 90,
            'startcolor' => array(
                'rgb' => '006039'
            ),
            'endcolor' => array(
                'argb' => '006039'
            )
        ),
);

$estiloCategorias[2] = array(
    'font' => array(
        'name'  => 'Antique Olive Compact',
        'bold'  => true,
        'size' =>20,
        'color' => array(
            'rgb' => '1F0F10'
        )
        ),
        'fill' => array(
            'type'       => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
      'rotation'   => 90,
            'startcolor' => array(
                'rgb' => '0099d8'
            ),
            'endcolor' => array(
                'argb' => '0099d8'
            )
        ),
);
$estiloCategorias[3] = array(
    'font' => array(
        'name'  => 'Antique Olive Compact',
        'bold'  => true,
        'size' =>20,
        'color' => array(
            'rgb' => '1F0F10'
        )
        ),
        'fill' => array(
            'type'       => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
      'rotation'   => 90,
            'startcolor' => array(
                'rgb' => 'db3128'
            ),
            'endcolor' => array(
                'argb' => 'db3128'
            )
        ),
);
$estiloCategorias[4] = array(
    'font' => array(
        'name'  => 'Antique Olive Compact',
        'bold'  => true,
        'size' =>20,
        'color' => array(
            'rgb' => '1F0F10'
        )
        ),
        'fill' => array(
            'type'       => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
      'rotation'   => 90,
            'startcolor' => array(
                'rgb' => 'dab61e'
            ),
            'endcolor' => array(
                'argb' => 'dab61e'
            )
        ),
);
$estiloCategorias[5] = array(
    'font' => array(
        'name'  => 'Antique Olive Compact',
        'bold'  => true,
        'size' =>20,
        'color' => array(
            'rgb' => '1F0F10'
        )
        ),
        'fill' => array(
            'type'       => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
      'rotation'   => 90,
            'startcolor' => array(
                'rgb' => '0c4370'
            ),
            'endcolor' => array(
                'argb' => '0c4370'
            )
        ),
);
$estiloCategorias[6] = array(
    'font' => array(
        'name'  => 'Antique Olive Compact',
        'bold'  => true,
        'size' =>20,
        'color' => array(
            'rgb' => '1F0F10'
        )
        ),
        'fill' => array(
            'type'       => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
      'rotation'   => 90,
            'startcolor' => array(
                'rgb' => 'ec6d23'
            ),
            'endcolor' => array(
                'argb' => 'ec6d23'
            )
        ),
);

$estiloCategorias[7] = array(
    'font' => array(
        'name'  => 'Antique Olive Compact',
        'bold'  => true,
        'size' =>20,
        'color' => array(
            'rgb' => '1F0F10'
        )
        ),
        'fill' => array(
            'type'       => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
      'rotation'   => 90,
            'startcolor' => array(
                'rgb' => 'a7a523'
            ),
            'endcolor' => array(
                'argb' => 'a7a523'
            )
        ),
);

$estiloCategorias[8] = array(
    'font' => array(
        'name'  => 'Antique Olive Compact',
        'bold'  => true,
        'size' =>20,
        'color' => array(
            'rgb' => '1F0F10'
        )
        ),
        'fill' => array(
            'type'       => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
      'rotation'   => 90,
            'startcolor' => array(
                'rgb' => 'c47cf2'
            ),
            'endcolor' => array(
                'argb' => 'FF431a5d'
            )
        ),
);

// Set document properties
$objPHPExcel->getProperties()->setCreator("Francisco Piedras Pérez")
							 ->setLastModifiedBy("Francisco Piedras Pérez")
							 ->setTitle("Actividades Trimestrales")
							 ->setSubject("Rozas Joven")
							 ->setDescription("Resumen de actividadedes para la concejalía de juventud en Las Rozas.")
							 ->setKeywords("Office 2017 actividades openxml php")
							 ->setCategory("Actividades");
$i = 2; //Comienzo de las filas quitando los títulos
$i_inicio =4;
$responsables = array();
$k = 0;

while ($categorias = $queryCategoria2->fetch(PDO::FETCH_ASSOC)) {
    // Añadimos el nombre de la categoria
    $queryCategoria->bindParam(':categoriaId',$categorias['idCategoria']);
    $queryCategoria->execute();
    $nombreCategoria = $queryCategoria->fetch();
    $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A'.$i,  $nombreCategoria['nombre']);
    $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':S'.$i)->applyFromArray($estiloCategorias[$categorias['idCategoria']]);
    $i++;

    $queryConsulta->bindParam(':id', $categorias['idCategoria']);
    $queryConsulta->execute();

    $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':S'.$i)->applyFromArray($estiloTituloColumnas);
    // CABECERA DE CADA CATEGORIA
    $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A'.$i,  $titulosColumnas[0])  //Titulo de las columnas
    ->setCellValue('B'.$i,  $titulosColumnas[1])
    ->setCellValue('C'.$i,  $titulosColumnas[2])
    ->setCellValue('D'.$i,  $titulosColumnas[3])
    ->setCellValue('E'.$i,  $titulosColumnas[4])
    ->setCellValue('F'.$i,  $titulosColumnas[5])
    ->setCellValue('G'.$i,  $titulosColumnas[6])
    ->setCellValue('H'.$i,  $titulosColumnas[7])
    ->setCellValue('I'.$i,  $titulosColumnas[8])
    ->setCellValue('J'.$i,  $titulosColumnas[9])
    ->setCellValue('K'.$i,  $titulosColumnas[10])
    ->setCellValue('L'.$i,  $titulosColumnas[11])
    ->setCellValue('M'.$i,  $titulosColumnas[12])
    ->setCellValue('N'.$i,  $titulosColumnas[13])
    ->setCellValue('O'.$i,  $titulosColumnas[14])
    ->setCellValue('P'.$i,  $titulosColumnas[15])
    ->setCellValue('Q'.$i,  $titulosColumnas[16])
    ->setCellValue('R'.$i,  $titulosColumnas[17])
    ->setCellValue('S'.$i,  $titulosColumnas[18])
    ->setCellValue('S'.$i,  $titulosColumnas[19]);
    $i++;
    while ($fila = $queryConsulta->fetch(PDO::FETCH_ASSOC)) {
        // Preparamos el tiempo
        $queryTiempo->bindParam(':id', $fila['idActividad']);
        $queryTiempo->execute();
        $tiempo = $queryTiempo->fetch(PDO::FETCH_ASSOC);

        //Buscamos el nombre del grupo
        $queryGrupo->bindParam(':grupoId',$fila['grupoId']);
        $queryGrupo->execute();
        $grupo = $queryGrupo->fetch(PDO::FETCH_ASSOC);
    
        //Buscamos el nombre del aula
        $queryAula->bindParam(':aulaId',$fila['aulaId']);
        $queryAula->execute();
        $aula = $queryAula->fetch(PDO::FETCH_ASSOC);
    
        // Buscamos la actividad responsable
        $queryActividadResponsable->bindParam(':actividadResponsableId', $fila['actividadResponsableId']);
        $queryActividadResponsable->execute();
        
        //Buscamos los responsables
        while( $r = $queryActividadResponsable->fetch(PDO::FETCH_ASSOC) ) {
            // Buscamos los responsables
            $queryResponsable->bindParam(':responsableId', $r['responsableId']);
            $queryResponsable->execute();
            $miResponsable = $queryResponsable->fetch(PDO::FETCH_ASSOC);
            $responsables[$k] = $miResponsable['nombre'];
            $k++;
        }
        $k = 0;
        // Pasamos el array a texto
        $responsablesTexto = implode(", ", $responsables);
        
    
        $objPHPExcel->setActiveSheetIndex(0)
             ->setCellValue('A'.$i, $fila['nombre'])
             ->setCellValue('B'.$i, $grupo['nombre'])
             ->setCellValue('C'.$i, $responsablesTexto)
             ->setCellValue('D'.$i, $fila['fechaInicio'])
             ->setCellValue('E'.$i, $fila['fechaFin'])
             ->setCellValue('F'.$i, $fila['dias'])
             ->setCellValue('G'.$i,$aula['nombre'])
             ->setCellValue('H'.$i, $fila['horaInicio'])
             ->setCellValue('I'.$i, $fila['horaFin'])
             ->setCellValue('J'.$i, $tiempo['HORAS'],'0' )
             ->setCellValue('K'.$i, $fila['precio'])
             ->setCellValue('L'.$i, $fila['parMin'])
             ->setCellValue('M'.$i, $fila['parMax'])
             ->setCellValue('N'.$i, $fila['edadMinima'])
             ->setCellValue('O'.$i, $fila['edadMaxima'])
             ->setCellValue('P'.$i, $fila['fechaInscripciones'])
             ->setCellValue('Q'.$i, $fila['incluye'])
             ->setCellValue('R'.$i, $fila['textoPrevioFolleto'])
             ->setCellValue('S'.$i, $fila['lugarCelebracion'])
             ->setCellValue('S'.$i, $fila['observaciones'])
            ;
        $objPHPExcel->getActiveSheet()->getStyle('J'.$i)
            ->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_TIME4);    
        $i++;
    }
    $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':S'.$i)->applyFromArray($estiloTotales);
    $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A'.$i, 'TOTALES')
    ->setCellValue('B'.$i, '=COUNTIF(B'.$i_inicio.':B'. ($i-1) .',"*")')
    ->setCellValue('J'.$i, '=SUM(J'.$i_inicio.':J'.($i-1).')');
    //->setCellValue(" B".$i, "=COUNTIF(B".$i_inicio.":B".$i.";'*')" );
    //var_dump('B'.$i, '=COUNTIF(B'.$i_inicio.':B'.$i.';"*")');
$i_inicio = $i+3;
$i++;
}

// Bloqueamos primera columna
$objPHPExcel->getActiveSheet()->freezePane('B1');

// Aplicamos ancho columnas
for($i = 'A'; $i <= 'S'; $i++){
    $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension($i)->setAutoSize(TRUE);
}

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Actividades');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="actividades.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
