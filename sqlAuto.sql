DROP DATABASE IF EXISTS `folleto`;

CREATE DATABASE IF NOT EXISTS `folleto` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci;
USE `folleto`;

USE folleto;

CREATE TABLE `aulas` (
  `idAula` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) NOT NULL,
  CONSTRAINT PK_AULA PRIMARY KEY (idAula)
);

CREATE TABLE `responsables` (
  `idResponsable` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) NOT NULL,
  CONSTRAINT PK_RESPONSABLE PRIMARY KEY (idResponsable)
);

CREATE TABLE `grupos` (
  `idGrupo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) NOT NULL,
  CONSTRAINT PK_GRUPO PRIMARY KEY (idGrupo)
);

CREATE TABLE `actividadesresponsables` (
  `idActividadResponsable` int(11) NOT NULL AUTO_INCREMENT,
  `responsableId` int(11) NOT NULL,
  CONSTRAINT PK_ACTIVIDADRESPONSABLE PRIMARY KEY (idActividadResponsable, responsableId),
  CONSTRAINT FK_ACTIVIDADRESPONSABLE FOREIGN KEY (`responsableId`) REFERENCES `responsables` (`idResponsable`)
);

CREATE TABLE `categorias` (
  `idCategoria` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) NOT NULL,
  CONSTRAINT PK_CATEGORIA PRIMARY KEY (idCategoria)
);

CREATE TABLE `actividades` (
  `idActividad` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) NOT NULL,
  `grupoId` int(11) NOT NULL,
  `fechaInicio` date NULL,
  `fechaFin` date NULL,
  `dias` varchar(60) NULL,
  `actividadResponsableId` int(11)  NULL,
  `precio` float  NULL,
  `horaInicio` time  NULL,
  `horaFin` time  NULL,
  `parMin` int(11)  NULL,
  `parMax` int(11)  NULL,
  `aulaId` int(11)  NULL,
  `edadMinima` int(11)  NULL,
  `edadMaxima` int(11)  NULL,
  `fechaInscripciones` date  NULL,
  `incluye` text NULL,
  `textoPrevioFolleto` text NULL,
  `lugarCelebracion` text NULL,
  `categoriaId` int(11)  NULL,
  `observaciones` text NULL,
  CONSTRAINT PK_ACTIVIDAD PRIMARY KEY (`idActividad`),
  CONSTRAINT FK_GRUPO FOREIGN KEY (`grupoId`) REFERENCES `grupos` (`idGrupo`),
  CONSTRAINT FK_ACTIVIDAD FOREIGN KEY (`actividadResponsableId`) REFERENCES `actividadesresponsables` (`idActividadResponsable`),
  CONSTRAINT FK_AULA FOREIGN KEY (`aulaId`) REFERENCES `aulas` (`idAula`),
  CONSTRAINT FK_CATEGORIA FOREIGN KEY (`categoriaId`) REFERENCES `categorias` (`idCategoria`)
);

INSERT INTO categorias (nombre) VALUES ('Aire libre y familia');
INSERT INTO categorias (nombre) VALUES ('Música y artes escénicas');
INSERT INTO categorias (nombre) VALUES ('Imagen y nuevas tecnologías');
INSERT INTO categorias (nombre) VALUES ('Habilidades domésticas');
INSERT INTO categorias (nombre) VALUES ('Formación y empleo');
INSERT INTO categorias (nombre)  VALUES ('Más ocio');
INSERT INTO categorias (nombre) VALUES ('Certámenes');

INSERT INTO responsables (nombre) VALUES ('Ana');
INSERT INTO responsables (nombre) VALUES ('Ángela');
INSERT INTO responsables (nombre) VALUES ('Carlos');
INSERT INTO responsables (nombre) VALUES ('Charlie');
INSERT INTO responsables (nombre) VALUES ('Cristina');
INSERT INTO responsables (nombre) VALUES ('Fito');
INSERT INTO responsables (nombre) VALUES ('Inés');
INSERT INTO responsables (nombre) VALUES ('J. Carlos');
INSERT INTO responsables (nombre) VALUES ('Jesús');
INSERT INTO responsables (nombre) VALUES ('Lourdes');
INSERT INTO responsables (nombre) VALUES ('Manuel');
INSERT INTO responsables (nombre) VALUES ('María');
INSERT INTO responsables (nombre) VALUES ('Trini');

INSERT INTO aulas (nombre) VALUES ( 'A1-Centro');
INSERT INTO aulas (nombre) VALUES ( 'B1-Centro');
INSERT INTO aulas (nombre) VALUES ( 'B2-Centro');
INSERT INTO aulas (nombre) VALUES ( 'B3-Centro');
INSERT INTO aulas (nombre) VALUES ( 'B4-Centro');

INSERT INTO aulas (nombre) VALUES ( 'Cocina-Centro');
INSERT INTO aulas (nombre) VALUES ( 'Salón de actos-Centro');
INSERT INTO aulas (nombre) VALUES ( 'Control 2-Centro');

INSERT INTO aulas (nombre) VALUES ( 'A1-Casa');
INSERT INTO aulas (nombre) VALUES ( 'A2-Casa');
INSERT INTO aulas (nombre) VALUES ( 'A3-Casa');
INSERT INTO aulas (nombre) VALUES ( 'A4-Casa');
INSERT INTO aulas (nombre) VALUES ( 'A5-Casa');
INSERT INTO aulas (nombre) VALUES ( 'A6-Casa');
INSERT INTO aulas (nombre) VALUES ( 'A7-Casa');

INSERT INTO aulas (nombre) VALUES ( 'A1-Matas');
INSERT INTO aulas (nombre) VALUES ( 'Salón de actos-Matas');

INSERT INTO aulas (nombre) VALUES ( 'EXTERIOR');

INSERT INTO grupos (nombre) VALUES ('Casa1');
INSERT INTO grupos (nombre) VALUES ('Casa2');
INSERT INTO grupos (nombre) VALUES ('Casa3');

INSERT INTO grupos (nombre) VALUES ('Centro1');
INSERT INTO grupos (nombre) VALUES ('Centro2');
INSERT INTO grupos (nombre) VALUES ('Centro3');
INSERT INTO grupos (nombre) VALUES ('Centro4');
INSERT INTO grupos (nombre) VALUES ('Centro5');

INSERT INTO grupos (nombre) VALUES ('Matas1');
INSERT INTO grupos (nombre) VALUES ('Matas2');
INSERT INTO grupos (nombre) VALUES ('Matas3');

INSERT INTO grupos (nombre) VALUES ('EXTERIOR1');
INSERT INTO grupos (nombre) VALUES ('EXTERIOR2');
INSERT INTO grupos (nombre) VALUES ('EXTERIOR3');

