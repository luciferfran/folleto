<?php

error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/Madrid');

if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');

include  'conexion.php';
$cn    = ConexionMySql();
/* SELECT * FROM actividades INNER JOIN aulas ON actividades.aulaId=aulas.idAula 
INNER JOIN categorias ON actividades.categoriaId=categorias.idCategoria
INNER JOIN grupos ON actividades.grupoId=grupos.idGrupo
INNER JOIN actividadesresponsables ON actividades.actividadResponsableId = actividadesresponsables.idActividadResponsable

*/
$consulta = "SELECT * FROM actividades ORDER BY categoriaId ASC";
// $consulta .= " INNER JOIN aulas ON actividades.aulaId=aulas.idAula";
// $consulta .= " INNER JOIN categorias ON actividades.categoriaId=categorias.idCategoria";
// $consulta .= " INNER JOIN grupos ON actividades.grupoId=grupos.idGrupo";
// $consulta .= " INNER JOIN actividadesresponsables ON actividades.actividadResponsableId = actividadesresponsables.idActividadResponsable";
$resultado = $cn->query($consulta);

// Diferencia de tiempos SQL SELECT timediff(horaFin, horaInicio) HORAS FROM actividades;
$sqlTiempo = 'SELECT timediff(horaFin, horaInicio) HORAS FROM actividades WHERE idActividad=:id';
$queryTiempo = $cn->prepare($sqlTiempo);

// Preparamos la consulta para sacar el grupo
$sqlGrupo = 'SELECT nombre FROM grupos WHERE idGrupo = :grupoId';
$queryGrupo = $cn->prepare($sqlGrupo);
// Preparamos la consulta para sacar el aula
$sqlAula = 'SELECT nombre FROM aulas WHERE idAula = :aulaId';
$queryAula = $cn->prepare($sqlAula);
// Preparamos la consulta para sacar los responsables de cada actividad
$sqlResponsable = 'SELECT nombre FROM responsables WHERE idResponsable = :responsableId';
$queryResponsable = $cn->prepare($sqlResponsable);
// Preparamos la consulta para sacar la categoria
$sqlCategoria = 'SELECT nombre FROM categorias WHERE idCategoria = :categoriaId';
$queryCategoria = $cn->prepare($sqlCategoria);
// Preparamos la consulta para sacar la actividadresponsable
$sqlActividadResponsable = 'SELECT responsableId FROM actividadesresponsables WHERE idActividadResponsable = :actividadResponsableId';
$queryActividadResponsable = $cn->prepare($sqlActividadResponsable);

//var_dump($resultado);
//var_dump($resultado->fetch());
/** Include PHPExcel */
//require_once dirname(__FILE__) . 'Classes\PHPExcel.php';
require_once 'Classes\PHPExcel.php';

$titulosColumnas = array('ACTIVIDAD','GRUPO','RESPONSABLE','FECHA INICIO','FECHA FIN',
'DIAS','AULA','HORARIO INICIO','HORARIO FIN', 'HORAS', 'PRECIO', 'PAR. MIN', 'PAR. MAX',	
'EDAD MIN',	'EDAD MAX',	'FECHA INSCRIPCIONES', 'INCLUYE', 'TEXTO PREVIO FOLLETO', 'LUGAR DE CELEBRACIÓN');


// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Francisco Piedras Pérez")
							 ->setLastModifiedBy("Francisco Piedras Pérez")
							 ->setTitle("Actividades Trimestrales")
							 ->setSubject("Rozas Joven")
							 ->setDescription("Resumen de actividadedes para la concejalía de juventud en Las Rozas.")
							 ->setKeywords("Office 2017 actividades openxml php")
							 ->setCategory("Actividades");


$objPHPExcel->setActiveSheetIndex(0)
->setCellValue('A1',  $titulosColumnas[0])  //Titulo de las columnas
->setCellValue('B1',  $titulosColumnas[1])
->setCellValue('C1',  $titulosColumnas[2])
->setCellValue('D1',  $titulosColumnas[3])
->setCellValue('E1',  $titulosColumnas[4])
->setCellValue('F1',  $titulosColumnas[5])
->setCellValue('G1',  $titulosColumnas[6])
->setCellValue('H1',  $titulosColumnas[7])
->setCellValue('I1',  $titulosColumnas[8])
->setCellValue('J1',  $titulosColumnas[9])
->setCellValue('K1',  $titulosColumnas[10])
->setCellValue('L1',  $titulosColumnas[11])
->setCellValue('M1',  $titulosColumnas[12])
->setCellValue('N1',  $titulosColumnas[13])
->setCellValue('O1',  $titulosColumnas[14])
->setCellValue('P1',  $titulosColumnas[15])
->setCellValue('Q1',  $titulosColumnas[16])
->setCellValue('R1',  $titulosColumnas[17])
->setCellValue('S1',  $titulosColumnas[18]);

$i = 2; //Numero de fila donde se va a comenzar a rellenar
$responsables = array();
$k = 0;
while ($fila = $resultado->fetch(PDO::FETCH_ASSOC)) {
    // Preparamos el tiempo
    $queryTiempo->bindParam(':id', $fila['idActividad']);
    $queryTiempo->execute();
    $tiempo = $queryTiempo->fetch(PDO::FETCH_ASSOC);
    //var_dump($tiempo);
    //Buscamos el nombre del grupo
    $queryGrupo->bindParam(':grupoId',$fila['grupoId']);
    $queryGrupo->execute();
    $grupo = $queryGrupo->fetch(PDO::FETCH_ASSOC);

    //Buscamos el nombre del aula
    $queryAula->bindParam(':aulaId',$fila['aulaId']);
    $queryAula->execute();
    $aula = $queryAula->fetch(PDO::FETCH_ASSOC);

    // Buscamos la actividad responsable
    $queryActividadResponsable->bindParam(':actividadResponsableId', $fila['actividadResponsableId']);
    $queryActividadResponsable->execute();
    
    //Buscamos los responsables
    while( $r = $queryActividadResponsable->fetch(PDO::FETCH_ASSOC) ) {
        // Buscamos los responsables
        $queryResponsable->bindParam(':responsableId', $r['responsableId']);
        $queryResponsable->execute();
        $miResponsable = $queryResponsable->fetch(PDO::FETCH_ASSOC);
        $responsables[$k] = $miResponsable['nombre'];
        $k++;
    }
    $k = 0;
    // Pasamos el array a texto
    $responsablesTexto = implode(", ", $responsables);
    
    /* Sacamos el cálculo de las horas si existe
    if( ($fila['horaInicio'] &&  $fila['horaFin']) != 0 ){
        $strStart = $fila['horaInicio']; 
        $strEnd   = $fila['horaFin']; 
     
        $dteStart = new DateTime($strStart); 
        $dteEnd   = new DateTime($strEnd); 
     
        $dteDiff  = $dteStart->diff($dteEnd); 
    }*/
   
    //var_dump($tiempo['HORAS']);

    $objPHPExcel->setActiveSheetIndex(0)
         ->setCellValue('A'.$i, $fila['nombre'])
         ->setCellValue('B'.$i, $grupo['nombre'])
         ->setCellValue('C'.$i, $responsablesTexto)
         ->setCellValue('D'.$i, $fila['fechaInicio'])
         ->setCellValue('E'.$i, $fila['fechaFin'])
         ->setCellValue('F'.$i, $fila['dias'])
         ->setCellValue('G'.$i,$aula['nombre'])
         ->setCellValue('H'.$i, $fila['horaInicio'])
         ->setCellValue('I'.$i, $fila['horaFin'])
         ->setCellValue('J'.$i, $tiempo['HORAS'])
         ->setCellValue('K'.$i, $fila['precio'])
         ->setCellValue('L'.$i, $fila['parMin'])
         ->setCellValue('M'.$i, $fila['parMax'])
         ->setCellValue('N'.$i, $fila['edadMinima'])
         ->setCellValue('O'.$i, $fila['edadMaxima'])
         ->setCellValue('P'.$i, $fila['fechaInscripciones'])
         ->setCellValue('Q'.$i, $fila['incluye'])
         ->setCellValue('R'.$i, $fila['textoPrevioFolleto'])
         ->setCellValue('S'.$i, $fila['lugarCelebracion'])
        // //->setCellValue('S'.$i, $fila['categoriaId'])
        ;
    $i++;
}

    // Aplicamos estilos
    $estiloTituloColumnas = array(
        'font' => array(
            'name'  => 'Arial',
            'bold'  => true,
            'color' => array(
                'rgb' => '000000'
            )
        ),
        'alignment' =>  array(
            'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            'wrap'      => TRUE
        )
    );
    $objPHPExcel->getActiveSheet()->getStyle('A1:S1')->applyFromArray($estiloTituloColumnas);

    // Aplicamos encho columnas
    for($i = 'A'; $i <= 'S'; $i++){
        $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension($i)->setAutoSize(TRUE);
    }

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Actividades');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="actividades.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
