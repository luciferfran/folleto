<!DOCTYPE html>
<html lang="es">

<head>
  <title>Ver actividad | Rozas Joven</title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb"
    crossorigin="anonymous">
  <link rel="stylesheet" href="../../css/jquery-ui.min.css">
  <link rel="stylesheet" href="../../css/jquery-ui.structure.min.css">
  <!-- <link rel="stylesheet" href="css/bootstrap-datepicker.standalone.css"> -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="../../css/bootstrap-datetimepicker.css">
  <link rel="stylesheet" href="../../css/jquery-ui.min.css">
</head>

<body>
  <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <a class="navbar-brand" href="#">Actividades Rozas Joven</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"
      aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item">
          <a class="nav-link" href="./">Inicio
            <span class="sr-only">(current)</span>
          </a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="aula.php">Aulas</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="categoria.php">Categorias</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="grupo.php">Grupos</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="responsable.php">Responsables</a>
        </li>

        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false">Nuevo</a>
          <div class="dropdown-menu mr-sm-2" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="acciones/insertar/insertarActividad.php">Actividad</a>
            <a class="dropdown-item" href="acciones/insertar/insertarAula.php">Aula</a>
            <a class="dropdown-item" href="acciones/insertar/insertarCategoria.php">Categoria</a>
            <a class="dropdown-item" href="acciones/insertar/insertarGrupo.php">Grupo</a>
            <a class="dropdown-item" href="acciones/insertar/insertarResponsable.php">Responsable</a>
          </div>
        </li>

      </ul>
    </div>
  </nav>
  <div class="container">
    <h1 class="text-center text-info">MI ACTIVIDAD</h1>
    <?php
    //include_once 'word-cabecera.php';
    require_once 'vendor/autoload.php';
    function getEndingNotes($writers,$nombreArchivo)
    {
        $result = '';
    
        // Do not show execution time for index
        $result .= date('H:i:s') . " Hecho guardado archivo(s)</br>";
        $result .= date('H:i:s') . " Memoria usada: " . (memory_get_peak_usage(true) / 1024 / 1024) . " MB</br>";
        
        $types = array_values($writers);
        $result .= '<p>&nbsp;</p>';
        $result .= '<p>Resultados: ';
        foreach ($types as $type) {
            if (!is_null($type)) {
                $resultFile =  'words/'.$nombreArchivo . '.' . $type;
                if (file_exists($resultFile)) {
                    $result .= "<a href='{$resultFile}' class='btn btn-primary'>{$type}</a> ";
                }
            }
        }
        $result .= '</p>';
    
        return $result;
    }
    
    $writers = array('Word2007' => 'docx', 'HTML' => 'html', 'PDF' => 'pdf');
    

    
    /*  DATOS DE LA ACTIVIDAD */
    include  'conexion.php';
    
    // Conexión
    $cn    = ConexionMySql();

    // Selección de actividad por parámetro
    $sql   = "SELECT * FROM actividades WHERE idActividad=:idActividad";
    $query = $cn->prepare($sql);
    $query->bindParam('idActividad', intval($_POST["idActividad"]));
    $query->execute();
    $r = $query->fetch(PDO::FETCH_ASSOC);
    
    // Selección responsables
    $sqlResponsable   = "SELECT nombre FROM responsables WHERE idResponsable = :idResponsable";
    $queryResponsable = $cn->prepare($sqlResponsable);
    
    // Selección aula
    $sqlAula = "SELECT nombre FROM aulas WHERE idAula=:aulaId";
    $queryAula = $cn->prepare($sqlAula);
    $queryAula->bindParam(':aulaId', $r['aulaId']);
    $queryAula->execute();
    $aula =$queryAula->fetch(PDO::FETCH_ASSOC);
    
    // Selección grupo
    $sqlGrupo = "SELECT nombre FROM grupos WHERE idGrupo=:grupoId";
    $queryGrupo = $cn->prepare($sqlGrupo);
    $queryGrupo->bindParam(':grupoId', $r['grupoId']);
    $queryGrupo->execute();
    $grupo =$queryGrupo->fetch(PDO::FETCH_ASSOC);
    
    // Selección categoias
    $sqlCategoria   = "SELECT nombre FROM categorias WHERE idCategoria=:categoriaId";
    $queryCategoria = $cn->prepare($sqlCategoria);
    $queryCategoria->bindParam(':categoriaId', $r['categoriaId']);
    $queryCategoria->execute();
    $categoria =$queryCategoria->fetch(PDO::FETCH_ASSOC);
    
    
    // Selección responsables dependiendo de la actividad
    $sqlActividadResponsable   = "SELECT * FROM actividadesResponsables WHERE idActividadResponsable = :actividadResponsableId";
    $queryActividadResponsable = $cn->prepare($sqlActividadResponsable);
    $queryActividadResponsable->bindParam(':actividadResponsableId', $r['actividadResponsableId']);
    $queryActividadResponsable->execute();

    // Cálculo del tiempo de la actividad
    $sqlTiempo = 'SELECT timediff(horaFin, horaInicio) HORAS FROM actividades WHERE idActividad=:id';
    $queryTiempo = $cn->prepare($sqlTiempo);
    $queryTiempo->bindParam(':id',$r['idActividad']);
    $queryTiempo->execute();
    $tiempo = $queryTiempo->fetch(PDO::FETCH_ASSOC);
    
    $encontrado = false;
    $contador = 0;
    $nombres = array();
    while ($s = $queryActividadResponsable->fetch(PDO::FETCH_ASSOC)) {
        $queryResponsable->bindParam('idResponsable',$s['responsableId']);
        $queryResponsable->execute();
        $j = $queryResponsable->fetch(PDO::FETCH_ASSOC);
        array_push($nombres,$j['nombre']);
    }
    // Template processor instance creation
echo date('H:i:s'), ' Creando nueva instancia de TemplateProcessor...</br>';
$templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor('words/plantilla.docx');

$phpWord = new \PhpOffice\PhpWord\PhpWord();


// Table with a spanned cell
$templateProcessor->setValue('nombre', $r['nombre']);
$templateProcessor->setValue('grupo', $grupo['nombre']);
$templateProcessor->setValue('categoria', $categoria['nombre']);
$templateProcessor->setValue('dias',  $r['dias']);
$templateProcessor->setValue('parMin',  $r['parMin']);
$templateProcessor->setValue('parMax', $r['parMax']);
$templateProcessor->setValue('edadMinima', $r['edadMinima']);
$templateProcessor->setValue('edadMaxima', $r['edadMaxima']);
$templateProcessor->setValue('fechaInscripcion', $r['fechaInscripciones']);
$templateProcessor->setValue('fechaInicio', $r['fechaInicio']);
$templateProcessor->setValue('fechaFin', $r['fechaFin']);
$templateProcessor->setValue('horaInicio', $r['horaInicio']);
$templateProcessor->setValue('horaFin', $r['horaFin']);
$templateProcessor->setValue('precio', $r['precio'].' €');
$templateProcessor->setValue('responsables', implode(",", $nombres));
$templateProcessor->setValue('ubicacion', $r['lugarCelebracion']);
$templateProcessor->setValue('incluye', $r['incluye']);
$templateProcessor->setValue('textoPrevio', $r['textoPrevioFolleto']);
$templateProcessor->setValue('observaciones', $r['observaciones']);
$templateProcessor->setValue('tiempo', $tiempo['HORAS']);

echo date('H:i:s'), ' Guardando el documento...</br>';

$templateProcessor->saveAs('words/'.$r['nombre'].'.docx');

//Busco el archivo vreado
$doc = PhpOffice\PhpWord\IOFactory::load('words/'.$r['nombre'].'.docx');
//Set the PDF Engine Renderer Path. Many engines are supported (TCPDF, DOMPDF, ...).
\PhpOffice\PhpWord\Settings::setPdfRendererPath('vendor/tecnickcom/tcpdf');
\PhpOffice\PhpWord\Settings::setPdfRendererName('TCPDF');
//Create a writer, which converts the PhpWord Object into an PDF
$xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($doc, 'PDF');
//Create again an temp file for the new generated PDF.
$pdf_temp = tmpfile();
$pdf_uri = stream_get_meta_data($pdf_temp)["uri"];

//Save the PDF to the path
$xmlWriter->save($pdf_uri);

//Now print the file from the temp path.
echo file_get_contents($pdf_uri);

echo getEndingNotes(array(
    'Word2007' => 'docx', 'PDF' => 'pdf'), $r['nombre']);
//header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
//header('Content-Disposition: attachment;filename=word.docx');
    ?>
<!--   </div>
  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
    crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh"
    crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ"
    crossorigin="anonymous"></script>
  <!-- <script src="js/bootstrap-datepicker.js"></script> -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.3/moment.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.3/locale/es.js"></script>
  <script src="../../js/bootstrap-datetimepicker.js"></script>
  <script src="../../js/tiempo.js"></script>
</body> -->