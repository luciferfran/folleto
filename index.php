<?php
include  'conexion.php';

$cn    = ConexionMySql();


if(isset($_POST['filtro'])){
    if($_POST['filtro'] == 0)
    {
        $sql   = "SELECT * FROM actividades ORDER BY categoriaId ASC";
        $query = $cn->query( $sql );
    }
    else{
        $sql = "SELECT * FROM actividades WHERE categoriaId = :filtro";
        $query = $cn->prepare($sql);
        $query->bindParam(':filtro', $_POST['filtro']);
        $query->execute();
    }
 
}
else{
    $sql   = "SELECT * FROM actividades ORDER BY categoriaId ASC";
    $query = $cn->query( $sql );
}

$sqlCategoria   = "SELECT * FROM categorias ORDER BY nombre ASC";
$queryCategoria = $cn->query( $sqlCategoria );
?>
    <!DOCTYPE html>
    <html lang="es">

    <head>
        <title>Visualización de datos | Rozas Joven</title>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb"
            crossorigin="anonymous">
        <link rel="stylesheet" href="css/jquery-ui.min.css">
        <link rel="stylesheet" href="css/jquery-ui.structure.min.css">
        <!-- <link rel="stylesheet" href="css/bootstrap-datepicker.standalone.css"> -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="css/bootstrap-datetimepicker.css">
        <link rel="stylesheet" href="css/jquery-ui.min.css">
        <link rel="stylesheet" href="css/style.css">
    </head>

    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
            <a class="navbar-brand" href="#">Actividades Rozas Joven</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href=".\">Inicio
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="aula.php">Aulas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="categoria.php">Categorias</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="grupo.php">Grupos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="responsable.php">Responsables</a>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">Nuevo</a>
                        <div class="dropdown-menu mr-sm-2" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="acciones/insertar/insertarActividad.php">Actividad</a>
                            <a class="dropdown-item" href="acciones/insertar/insertarAula.php">Aula</a>
                            <a class="dropdown-item" href="acciones/insertar/insertarCategoria.php">Categoria</a>
                            <a class="dropdown-item" href="acciones/insertar/insertarGrupo.php">Grupo</a>
                            <a class="dropdown-item" href="acciones/insertar/insertarResponsable.php">Responsable</a>
                        </div>
                    </li>

                </ul>
            </div>
        </nav>
        <div class="container-fluid">
            <h1 class="text-center text-success">Visualizacion de datos</h1>
            <form class="form-inline justify-content-center" action="./" method="post">
                <div class="form-row">
                    <div class="input-group align-items-center">
                        <label class="p-2 text-primary font-weight-bold" for="filtro">CATEGORIAS</label>
                        <select class="form-control" id="filtro" name="filtro">
                        <option value="0">TODOS</option>
                        <?php
                          while( $r = $queryCategoria->fetch() ) {
                        ?>
                        <option value="<?php echo intval($r[0]) ?>"><?php echo $r[1]?></option>
                        <?php } ?>
                        </select>
                    </div>

                    <div class="col-auto">
                        <button type="submit" class="btn btn-primary">Filtar</button>
                    </div>
                  
                    <a class="btn btn-secondary" href="excel.php"> <i class="fa  fa-download"></i> EXCEL</a>
                    <a class="btn btn-secondary" href="pdfTodos.php"> <i class="fa  fa-download"></i> PDFs</a>
                    
                    <a class="btn btn-danger float-right" href="acciones/borrar/borrarTodo.php"> <i class="fa  fa-trash"></i> BORRAR TODO</a>
                </div>
            </form>
            <br>
            <div class="horizontal">
                <table id="tabla" class="table table-bordered">
                    <thead class="thead-dark">
                        <tr class="text-center">
                            <th class="align-middle">#</th>
                            <th class="align-middle">Actividad</th>
                            <th class="align-middle">Categoria</th>
                            <th class="align-middle">Grupo</th>
                            <th class="align-middle">Aula</th>
                            <th class="align-middle">Responsable(s)</th>
                            <th class="align-middle">Fecha de inicio</th>
                            <th class="align-middle">Fecha de fin</th>
                            <th class="align-middle">Días</th>
                            <th class="align-middle">Precio</th>
                            <th class="align-middle">Hora de inicio</th>
                            <th class="align-middle">Hora de fin</th>
                            <th class="align-middle">Par. Mín</th>
                            <th class="align-middle">Par. Máx</th>
                            <th class="align-middle">Edad mín</th>
                            <th class="align-middle">Edad máx</th>
                            <th class="align-middle">Fecha incripcion</th>
                            <th class="align-middle">Incluye</th>
                            <th class="align-middle">Texto</th>
                            <th class="align-middle">Lugar</th>
                            <th class="align-middle">Observaciones</th>
                            <th class="align-middle">Editar</th>
                            <th class="align-middle">Borrar</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                    while( $r = $query->fetch(PDO::FETCH_ASSOC) ) {
                ?>
                            <tr class="text-center <?php 
                                switch ($r['categoriaId']) {
                                    case '1':
                                        echo 'aire';
                                        break;
                                    case '2':
                                        echo 'musica';
                                        break;
                                    case '3':
                                        echo 'imagen';
                                        break;
                                    case '4':
                                        echo 'habilidades';
                                        break;
                                    case '5':
                                        echo 'formacion';
                                        break;
                                    case '6':
                                        echo 'ocio';
                                        break;
                                    case '7';
                                        echo 'certamen';
                                        break;
                                    default:
                                        echo 'desconocido';
                                        break;
                                };?>">
                                <td>
                                    <?php echo '<a class="text-white" href="word.php?idActividad='.$r["idActividad"].'"><i class="fa fa-file-text"></i></a>
                                    <a class="text-danger" href="pdf.php?idActividad='.$r["idActividad"].'"><i class="fa fa-file-pdf-o"></i></a>' ?>
                                </td>
                                <td>
                                    <?php echo '<a href="actividad.php?id='.$r["idActividad"].'">'.$r['nombre'].'</a>' ?>
                                </td>
                                <td>
                                    <?php 
                                $queryCategoria = $cn->prepare('SELECT nombre FROM categorias WHERE idCategoria = :categoriaId');
                                $queryCategoria -> bindParam(':categoriaId', $r['categoriaId']);
                                $queryCategoria -> execute();
                                $c =  $queryCategoria->fetch(PDO::FETCH_ASSOC);
                                echo $c['nombre']?>
                                </td>
                                <td>
                                    <?php 
                            $queryGrupo = $cn->prepare('SELECT nombre FROM grupos WHERE idGrupo = :grupoId');
                            $queryGrupo -> bindParam(':grupoId', $r['grupoId']);
                            $queryGrupo -> execute();
                            $g =  $queryGrupo->fetch(PDO::FETCH_ASSOC);
                            echo $g['nombre']?>
                                </td>
                                <td>
                                    <?php
                            $queryAula = $cn -> prepare('SELECT nombre FROM aulas WHERE idAula = :aulaId');
                            $queryAula -> bindParam(':aulaId', $r['aulaId']);
                            $queryAula -> execute();
                            $a =  $queryAula->fetch(PDO::FETCH_ASSOC);
                            echo $a['nombre']?>
                                </td>
                                <td>
                                    <?php
                            $contador = FALSE;

                            $queryActividadesResponsables = $cn->prepare('SELECT responsableId FROM actividadesResponsables WHERE idActividadResponsable = :actividadResponsableId');
                            $queryActividadesResponsables -> bindParam(':actividadResponsableId', $r["actividadResponsableId"]);
                            $queryActividadesResponsables -> execute();
                                                                
                            $queryNombre = $cn->prepare('SELECT nombre FROM responsables WHERE idResponsable = :responsableId');
                            
                            while($n = $queryActividadesResponsables->fetch(PDO::FETCH_ASSOC))
                            {  
                                $queryNombre->bindParam(':responsableId', $n['responsableId']);
                                $queryNombre->execute();
                                
                                while($s = $queryNombre->fetch(PDO::FETCH_ASSOC))
                                {  
                                    if ($contador == FALSE){
                                        $contador = TRUE;
                                        echo $s['nombre'];
                                    }
                                    else{
                                        echo ', ' . $s['nombre'] ;
                                    }
                                }   
                            } 
                        ?>
                                </td>
                                <td>
                                    <?php echo $r['fechaInicio']?>
                                </td>
                                <td>
                                    <?php echo $r['fechaFin']?>
                                </td>
                                <td>
                                    <?php echo $r['dias']?>
                                </td>
                                <td>
                                    <?php echo $r['precio']?>
                                </td>
                                <td>
                                    <?php echo substr($r['horaInicio'], 0, 5)?>
                                </td>
                                <td>
                                    <?php echo substr($r['horaFin'], 0, 5)?>
                                </td>
                                <td>
                                    <?php echo $r['parMin']?>
                                </td>
                                <td>
                                    <?php echo $r['parMax']?>
                                </td>
                                <td>
                                    <?php echo $r['edadMinima']?>
                                </td>
                                <td>
                                    <?php echo $r['edadMaxima']?>
                                </td>
                                <td>
                                    <?php echo $r['fechaInscripciones']?>
                                </td>
                                <td>
                                    <?php echo $r['incluye']?>
                                </td>
                                <td>
                                    <?php echo $r['textoPrevioFolleto']?>
                                </td>
                                <td>
                                    <?php echo $r['lugarCelebracion']?>
                                </td>
                                <td>
                                    <?php echo $r['observaciones']?>
                                </td>
                                <td>
                                    <a href=<?php echo 'acciones/editar/editarActividad.php?id=' . $r[ 'idActividad']; ?>><i class="fa  fa-pencil"></i> Editar</a>
                                </td>
                                <td>
                                    <a class="text-danger" href=<?php echo 'acciones/borrar/borrarActividad.php?id=' . $r[ 'idActividad']; ?>><i class="fa  fa-trash"></i> Borrar</a>
                                </td>
                                <?php } ?>
                            </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <?php include 'includes/footer.php';?>
            <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
      crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh"
      crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ"
      crossorigin="anonymous"></script>
    <!-- <script src="js/bootstrap-datepicker.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.3/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.3/locale/es.js"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="../../js/bootstrap-datetimepicker.js"></script>
    <script src="../../js/tiempo.js"></script>
    </body>

    </html>