<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Nueva actividad | Rozas Joven</title>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb"
            crossorigin="anonymous">
        <link rel="stylesheet" href="css/jquery-ui.min.css">
        <link rel="stylesheet" href="css/jquery-ui.structure.min.css">
        <!-- <link rel="stylesheet" href="css/bootstrap-datepicker.standalone.css"> -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="css/bootstrap-datetimepicker.css">
        <link rel="stylesheet" href="css/jquery-ui.min.css">
    </head>

    <body>
        <div class="container">
            <br>
<?php
include('../../conexion.php');
$cn = ConexionMySql();
$cn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  // iniciar transacción 
$cn->beginTransaction();
try{
  //  Guardamos los responsables primero
  $sentenciaActividadResponsable = $cn->prepare("INSERT INTO actividadesresponsables (idActividadResponsable, responsableId) VALUES (:idActividadResponsable, :responsableId)");
  $sentenciaActividadResponsable2 = $cn->prepare("INSERT INTO actividadesresponsables (responsableId) VALUES (:responsableId)");
  
  $primera=0;
  foreach ($_POST["actividadResponsableId"] as $selectedOption){
    if($primera == 0){
      $primera = 1;
      $sentenciaActividadResponsable2->bindParam(':responsableId',  $selectedOption);
      $sentenciaActividadResponsable2->execute();
      $id = $cn->lastInsertId();
    }
    else{ 
      $sentenciaActividadResponsable->bindParam(':idActividadResponsable', $id);
      $sentenciaActividadResponsable->bindParam(':responsableId', $selectedOption);
      $sentenciaActividadResponsable->execute();
    }
  }
  //  Guardamos la actividad
  $sentencia = $cn->prepare("INSERT INTO actividades 
  (fechaInicio, fechaFin, nombre, grupoId, actividadResponsableId, precio, horaInicio, horaFin, parMin, parMax, aulaId, edadMinima, edadMaxima, fechaInscripciones, incluye, textoPrevioFolleto,lugarCelebracion, dias, categoriaId, observaciones ) 
  VALUES (:fechaInicio, :fechaFin, :nombre, :grupoId, :actividadResponsableId, :precio, :horaInicio, :horaFin, :parMin, :parMax, :aulaId, :edadMinima, :edadMaxima, :fechaInscripciones, :incluye, :textoPrevioFolleto, :lugarCelebracion, :dias, :categoriaId, :observaciones)");
  $sentencia->bindParam(':fechaInicio', $_POST["fechaInicio"]);
  $sentencia->bindParam(':fechaFin', $_POST["fechaFin"]);
  $sentencia->bindParam(':nombre', $_POST["nombre"]);
  $sentencia->bindParam(':grupoId', $_POST["grupoId"]);
  $sentencia->bindParam(':aulaId', $_POST["aulaId"]);
  $sentencia->bindParam(':actividadResponsableId', $id);
  $sentencia->bindParam(':precio', $_POST["precio"]);
  $sentencia->bindParam(':horaInicio', $_POST["horaInicio"]);
  $sentencia->bindParam(':horaFin', $_POST["horaFin"]);
  $sentencia->bindParam(':parMin', $_POST["parMin"]);
  $sentencia->bindParam(':parMax', $_POST["parMax"]);
  $sentencia->bindParam(':aulaId', $_POST["aulaId"]);
  $sentencia->bindParam(':edadMinima', $_POST["edadMinima"]);
  $sentencia->bindParam(':edadMaxima', $_POST["edadMaxima"]);
  $sentencia->bindParam(':fechaInscripciones', $_POST["fechaInscripciones"]);
  $sentencia->bindParam(':incluye', $_POST["incluye"]);
  $sentencia->bindParam(':textoPrevioFolleto', $_POST["textoPrevioFolleto"]);
  $sentencia->bindParam(':lugarCelebracion', $_POST["lugarCelebracion"]);
  $sentencia->bindParam(':dias', $_POST["dias"]);
  $sentencia->bindParam(':categoriaId', $_POST["categoriaId"]);
  $sentencia->bindParam(':observaciones',$_POST['observaciones']);
  $sentencia->execute(); 

  // Hacemos el commit
  $cn->commit();
  header( "refresh:2; url=../../" );
  echo '
  <div class="container">
    <br>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>¡Actividad creada!</strong> Serás redirigido en 2 segundos.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
  </div>
  ';
} catch (PDOException $e) { 
  // si ocurre un error hacemos rollback para anular todos los insert 
  $cn->rollback();
  header( "refresh:2; url=../insertar/insertarActividad.php" ); 
  echo '
    <div class="container">
      <br>
      <div class="alert alert-warning alert-dismissible fade show" role="alert">
          <strong>!ERROR Activdad no creada!</strong> Serás redirigido en 2 segundos.
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button>
      </div>
    </div>
  '; 
  } 
?>
        </div>
    </body>
</html>