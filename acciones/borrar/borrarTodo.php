<?php
include  '../../conexion.php';

// Conexión
$cn    = ConexionMySql();

// Selección responsables
$sqlResponsable   = "SELECT * FROM responsables ORDER BY nombre ASC";
$queryResponsable = $cn->query( $sqlResponsable );

// Selección aulas
$sqlAula = "SELECT * FROM aulas ORDER BY nombre ASC";
$queryAula = $cn->query( $sqlAula );

// Selección gurpos
$sqlGrupo = "SELECT * FROM grupos ORDER BY nombre ASC";
$queryGrupo = $cn->query( $sqlGrupo );

// Selección categoias
$sqlCategoria   = "SELECT * FROM categorias ORDER BY nombre ASC";
$queryCategoria = $cn->query( $sqlCategoria );

// Selección de actividad por parámetro
$sql   = "SELECT * FROM actividades WHERE idActividad=:idActividad";
$query = $cn->prepare($sql);
$query->bindParam('idActividad', $_GET["id"]);
$query->execute();
$r = $query->fetch(PDO::FETCH_ASSOC);

// Selección responsables dependiendo de la actividad
$sqlActividadResponsable   = "SELECT * FROM actividadesResponsables WHERE idActividadResponsable = :actividadResponsableId";
$queryActividadResponsable = $cn->prepare($sqlActividadResponsable);
$queryActividadResponsable->bindParam(':actividadResponsableId', $r['actividadResponsableId']);
$queryActividadResponsable->execute();
//$t = $queryActividadResponsable->fetchAll(PDO::FETCH_ASSOC);

//
$sqlResponsable2   = "SELECT idResponsable FROM responsables WHERE idResponsable=:responsableId";
$queryResponsable2 = $cn->prepare($sqlResponsable2);

?>
  <!DOCTYPE html>
  <html lang="es">

  <head>
    <title>Borrar actividad | Rozas Joven</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb"
      crossorigin="anonymous">
    <link rel="stylesheet" href="../../css/jquery-ui.min.css">
    <link rel="stylesheet" href="../../css/jquery-ui.structure.min.css">
    <!-- <link rel="stylesheet" href="css/bootstrap-datepicker.standalone.css"> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../css/bootstrap-datetimepicker.css">
    <link rel="stylesheet" href="../../css/jquery-ui.min.css">
    <link rel="stylesheet" href="../../css/style.css">
  </head>

  <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
      <a class="navbar-brand" href="#">Borrar todas las actividades Rozas Joven</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"
        aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="../../">Inicio
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="../../aula.php">Aulas</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../../categoria.php">Categorias</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../../grupo.php">Grupos</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../../responsable.php">Responsables</a>
          </li>

          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
              aria-expanded="false">Nuevo</a>
            <div class="dropdown-menu mr-sm-2" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="../insertar/insertarActividad.php">Actividad</a>
              <a class="dropdown-item" href="../insertar/insertarAula.php">Aula</a>
              <a class="dropdown-item" href="../insertar/insertarCategoria.php">Categoria</a>
              <a class="dropdown-item" href="../insertar/insertarGrupo.php">Grupo</a>
              <a class="dropdown-item" href="../insertar/insertarResponsable.php">Responsable</a>
            </div>
          </li>

        </ul>
      </div>
    </nav>
    <div class="container">
      <h1 class="text-center text-warning">BORRAR TODAS LAS ACTIVIDADES</h1>
      <form  method="POST" action="borrarKo.php">
      <div class="form-group text-center col-12">
          <input type="hidden" name="idActividad" id="idActividad"  value="<?php echo $r['idActividad']?>">
          <input  class="btn btn-danger float-center" type=button onclick="if(confirm('¿Estás seguro de borrar todas las actividades?')){
                                                                          this.form.submit();}
                                                                          else{ alert('Operacion Cancelada');}"
          value="Borrar TODO">
      </div>
      <div class="text-center text-danger">
        <h1>ATENCIÓN SI CONTINUAS BORRARÁS TODAS LAS ACTIVIDADES Y NO HAY VUELTA ATRÁS</h1>
      </div>
      </form>
    </div>
    <?php include '../../includes/footer.php';?>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
      crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh"
      crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ"
      crossorigin="anonymous"></script>
    <!-- <script src="js/bootstrap-datepicker.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.3/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.3/locale/es.js"></script>
    <script src="../../js/bootstrap-datetimepicker.js"></script>
    <script src="../../js/tiempo.js"></script>
  </body>