<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Borrando actividad | Rozas Joven</title>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb"
            crossorigin="anonymous">
        <link rel="stylesheet" href="css/jquery-ui.min.css">
        <link rel="stylesheet" href="css/jquery-ui.structure.min.css">
        <!-- <link rel="stylesheet" href="css/bootstrap-datepicker.standalone.css"> -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="css/bootstrap-datetimepicker.css">
        <link rel="stylesheet" href="css/jquery-ui.min.css">
    </head>

    <body>
        <div class="container">
            <br>
<?php
include('../../conexion.php');
$cn = ConexionMySql();
$cn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
// iniciar transacción 
$cn->beginTransaction();
try{
    //  Eliminamos la acticidad en sí
  $sentencia = $cn->prepare("DELETE FROM actividades WHERE idActividad = :idActividad");
  $sentencia->bindParam(':idActividad', $_POST["idActividad"]);
  $sentencia->execute(); 

    //  Eliminamos actividadesresponsables asociada
  $sentenciaActividadResponsable = $cn->prepare("DELETE FROM actividadesresponsables WHERE idActividadResponsable = :actividadResponsableId");
  $sentenciaActividadResponsable->bindParam(':actividadResponsableId',$_POST['actividadResponsableId']);
  $sentenciaActividadResponsable->execute();


  // Hacemos el commit
  $cn->commit();
  header( "refresh:2; url=../../" );
  echo '
  <div class="container">
    <br>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>¡Actividad correctamente eliminada!</strong> Serás redirigido en 2 segundos.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
  </div>
  ';
} catch (PDOException $e) { 
  // si ocurre un error hacemos rollback para anular todos los insert 
  $cn->rollback();
  //header( "refresh:2; url=../../" ); 
  echo '
    <div class="container">
      <br>
      <div class="alert alert-warning alert-dismissible fade show" role="alert">
          <strong>!ERROR Activdad no borrada!</strong> Serás redirigido en 2 segundos.
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button>
      </div>
    </div>
  '; 
  var_dump($e);
  } 
?>
        </div>
    </body>
</html>