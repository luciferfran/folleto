<?php
include  '../../conexion.php';

$cn    = ConexionMySql();
$sqlResponsable   = "SELECT * FROM responsables ORDER BY nombre ASC";
$queryResponsable = $cn->query( $sqlResponsable );

$sqlAula = "SELECT * FROM aulas ORDER BY nombre ASC";
$queryAula = $cn->query( $sqlAula );

$sqlGrupo = "SELECT * FROM grupos ORDER BY nombre ASC";
$queryGrupo = $cn->query( $sqlGrupo );

$sqlCategoria   = "SELECT * FROM categorias ORDER BY nombre ASC";
$queryCategoria = $cn->query( $sqlCategoria );
/* while( $r = $queryResponsable->fetch() ) {
    echo json_encode( $r );
} */


?>

  <!doctype html>
  <html lang="es">

  <head>
    <title>Introducción de datos | Rozas Joven</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb"
      crossorigin="anonymous">
    <link rel="stylesheet" href="../../css/jquery-ui.min.css">
    <link rel="stylesheet" href="../../css/jquery-ui.structure.min.css">
    <!-- <link rel="stylesheet" href="css/bootstrap-datepicker.standalone.css"> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../css/bootstrap-datetimepicker.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="../../css/jquery-ui.min.css">
    <link rel="stylesheet" href="../../css/style.css">
  </head>

  <body>
      <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
          <a class="navbar-brand" href="#">Actividades Rozas Joven</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"
            aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
          <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item">
                <a class="nav-link" href="../../">Inicio <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="../../aula.php">Aulas</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="../../categoria.php">Categorias</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="../../grupo.php">Grupos</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="../../responsable.php">Responsables</a>
              </li>
  
              <li class="nav-item dropdown active">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
                  aria-expanded="false">Nuevo</a>
                <div class="dropdown-menu mr-sm-2" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="#">Actividad</a>
                  <a class="dropdown-item" href="insertarAula.php">Aula</a>
                  <a class="dropdown-item" href="insertarCategoria.php">Categoria</a>
                  <a class="dropdown-item" href="insertarGrupo.php">Grupo</a>
                  <a class="dropdown-item" href="insertarResponsable.php">Responsable</a>
                </div>
              </li>
  
            </ul>
          </div>
        </nav>
    <div class="container">
      <h1 class="text-center text-success">Datos de la actividad</h1>
      <form action="../crear/crearActividad.php" method="post">
        <!-- PRIMERA FILA -->
        <div class="form-group">
          <label for="nombre">Actividad</label>
          <input type="text" class="form-control" id="nombre" name="nombre">
        </div>
        <!-- SEGUNDA FILA -->
        <div class="form-row">
          <div class="col-md-3">
            <div class="form-group">
              <label for="grupoId">Grupo</label>
              <select class="form-control" id="grupoId" name="grupoId">
              <?php
                while( $r = $queryGrupo->fetch() ) {
              ?>
              <option value="<?php echo intval($r[0]) ?>"><?php echo $r[1]?></option>
              <?php } ?>
              </select>
            </div>
            <div class="form-group">
              <label for="aulaId">Aula</label>
              <select class="form-control" id="aulaId" name="aulaId">
              <?php
                while( $r = $queryAula->fetch() ) {
              ?>
                <option value="<?php echo intval($r[0]) ?>"><?php echo $r[1]?></option>
              <?php } ?>
              </select>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="actividadResponsableId">Responsable</label>
              <select multiple class="form-control" id="actividadResponsableId" name="actividadResponsableId[]">
          <?php
          while( $r = $queryResponsable->fetch() ) {
              ?>
            <option value="<?php echo intval($r[0]) ?>"><?php echo $r[1]?></option>
          <?php } ?>
          </select>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="fechaInicio">Fecha de inicio</label>
              <br>
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span><input class="fecha"
                  type="text" name="fechaInicio" id="fechaInicio">
              </div>
            </div>
            <div class="form-group">
              <label for="fechaFin">Fecha de fin</label>
              <br>
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span><input class="fecha"
                  type="text" name="fechaFin" id="fechaFin">
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="dias">Días</label>
              <br>
              <input type="text" name="dias" id="dias" pattern="[LMXJVSD,]{1,}" title="Sólo válido L M X J V S D separados por comas">
            </div>
            <div class="form-group">
              <label for="precio">Precio</label>
              <br>
              <input type="number" step="1" min=0 name="precio" id="precio">
            </div>
          </div>
        </div>
        <!-- TERCERA FILA -->
        <div class="form-row">
          <div class="col-md-3">
            <div class="form-group">
              <label for="horaInicio">Hora de inicio</label>
              <br>
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-clock-o" aria-hidden="true"></i></span><input class="hora"
                  type="text" name="horaInicio" id="horaInicio">
              </div>
            </div>
            <div class="form-group">
              <label for="horaFin">Hora de fin</label>
              <br>
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-clock-o" aria-hidden="true"></i></span><input class="hora"
                  type="text" name="horaFin" id="horaFin">
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="parMin">Par.Min.</label>
              <br>
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-users" aria-hidden="true"></i></span><input type="number"
                  id="parMin" name="parMin">
              </div>
            </div>
            <div class="form-group">
              <label for="parMax">Par.Max.</label>
              <br>
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-users" aria-hidden="true"></i></span><input type="number"
                  id="parMax" name="parMax">
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="edadMinima">Edad mínima</label>
              <br>
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-birthday-cake" aria-hidden="true"></i></span><input type="number"
                  name="edadMinima" id="edadMiiman">
              </div>
            </div>
            <div class="form-group">
              <label for="edadMaxima">Edad máxima</label>
              <br>
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-birthday-cake" aria-hidden="true"></i></span><input type="number"
                  name="edadMaxima" id="edadMaxima">
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="fechaInscripciones">Fecha de inscripciones</label>
              <br>
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span><input class="fecha"
                  type="text" name="fechaInscripciones" id="fechaInscripciones" />
              </div>
            </div>
            <div class="form-group">
              <label for="categoriaId">Categoria/Sección</label>
              <select class="form-control" id="categoriaId" name="categoriaId">
              <?php
                while( $r = $queryCategoria->fetch() ) {
              ?>
              <option value="<?php echo intval($r[0]) ?>"><?php echo $r[1]?></option>
              <?php } ?>
              </select>
            </div>
          </div>
        </div>
        <!-- CUARTA FILA -->
        <div class="form-row">
          <div class="form-group col-md-12">
            <label for="incluye">Incluye</label>
            <textarea class="form-control" name="incluye" id="incluye" rows="2"></textarea>
          </div>
          <div class="form-group col-md-12">
            <label for="textoPrevioFolleto">Texto previo folleto</label>
            <textarea class="form-control" name="textoPrevioFolleto" id="textoPrevioFolleto" rows="2" required></textarea>
          </div>
          <div class="form-group col-md-12">
            <label for="lugarCelebracion">Lugar de la celebración</label>
            <input type="text" class="form-control" name="lugarCelebracion" id="lugarCelebracion"  />
          </div>
          <div class="form-group col-md-12">
            <label for="observaciones">Observaciones</label>
            <textarea class="form-control" name="observaciones" id="observaciones" rows="2" ></textarea>
          </div>
        </div>
        <div class="form-group">
          <input class="btn btn-primary float-right" type="submit" value="Guardar">
          <br>
        </div>
      </form>
      <br>
    </div>
    <?php include '../../includes/footer.php';?>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
      crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh"
      crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ"
      crossorigin="anonymous"></script>
    <!-- <script src="js/bootstrap-datepicker.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.3/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.3/locale/es.js"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="../../js/bootstrap-datetimepicker.js"></script>
    <script src="../../js/tiempo.js"></script>
  </body>

  </html>