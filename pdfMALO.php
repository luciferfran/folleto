<?php
  require_once 'vendor/autoload.php';
  function getEndingNotes($writers,$nombreArchivo)
  {
    $result = '';
    $types = array_values($writers);
    foreach ($types as $type) {
      if (!is_null($type)) {
        $resultFile =  'words/'.$nombreArchivo . '.' . $type;
          if (file_exists($resultFile)) {
            $result .= "<a href='{$resultFile}' class='btn btn-primary'>{$type}</a> ";
          }
      }
    }
    return $result;
    }
    
    $writers = array('Word2007' => 'docx', 'HTML' => 'html', 'PDF' => 'pdf');
    
    
    /*  DATOS DE LA ACTIVIDAD */
    include  'conexion.php';
    
    // Conexión
    $cn    = ConexionMySql();

    // Selección de actividad por parámetro
    $sql   = "SELECT * FROM actividades WHERE idActividad=:idActividad";
    $query = $cn->prepare($sql);
    $query->bindParam('idActividad', intval($_GET["idActividad"]));
    $query->execute();
    $r = $query->fetch(PDO::FETCH_ASSOC);
    
    // Selección responsables
    $sqlResponsable   = "SELECT nombre FROM responsables WHERE idResponsable = :idResponsable";
    $queryResponsable = $cn->prepare($sqlResponsable);
    
    // Selección aula
    $sqlAula = "SELECT nombre FROM aulas WHERE idAula=:aulaId";
    $queryAula = $cn->prepare($sqlAula);
    $queryAula->bindParam(':aulaId', $r['aulaId']);
    $queryAula->execute();
    $aula =$queryAula->fetch(PDO::FETCH_ASSOC);
    
    // Selección grupo
    $sqlGrupo = "SELECT nombre FROM grupos WHERE idGrupo=:grupoId";
    $queryGrupo = $cn->prepare($sqlGrupo);
    $queryGrupo->bindParam(':grupoId', $r['grupoId']);
    $queryGrupo->execute();
    $grupo =$queryGrupo->fetch(PDO::FETCH_ASSOC);
    
    // Selección categoias
    $sqlCategoria   = "SELECT nombre FROM categorias WHERE idCategoria=:categoriaId";
    $queryCategoria = $cn->prepare($sqlCategoria);
    $queryCategoria->bindParam(':categoriaId', $r['categoriaId']);
    $queryCategoria->execute();
    $categoria =$queryCategoria->fetch(PDO::FETCH_ASSOC);
    
    
    // Selección responsables dependiendo de la actividad
    $sqlActividadResponsable   = "SELECT * FROM actividadesResponsables WHERE idActividadResponsable = :actividadResponsableId";
    $queryActividadResponsable = $cn->prepare($sqlActividadResponsable);
    $queryActividadResponsable->bindParam(':actividadResponsableId', $r['actividadResponsableId']);
    $queryActividadResponsable->execute();

    // Cálculo del tiempo de la actividad
    $sqlTiempo = 'SELECT timediff(horaFin, horaInicio) HORAS FROM actividades WHERE idActividad=:id';
    $queryTiempo = $cn->prepare($sqlTiempo);
    $queryTiempo->bindParam(':id',$r['idActividad']);
    $queryTiempo->execute();
    $tiempo = $queryTiempo->fetch(PDO::FETCH_ASSOC);
    
    $encontrado = false;
    $contador = 0;
    $nombres = array();
    while ($s = $queryActividadResponsable->fetch(PDO::FETCH_ASSOC)) {
        $queryResponsable->bindParam('idResponsable',$s['responsableId']);
        $queryResponsable->execute();
        $j = $queryResponsable->fetch(PDO::FETCH_ASSOC);
        array_push($nombres,$j['nombre']);
    }
    // Template processor instance creation
//echo date('H:i:s'), ' Creando nueva instancia de TemplateProcessor...</br>';
$templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor('words/plantilla.docx');

$phpWord = new \PhpOffice\PhpWord\PhpWord();


// Table with a spanned cell
$templateProcessor->setValue('nombre', $r['nombre']);
$templateProcessor->setValue('grupo', $grupo['nombre']);
$templateProcessor->setValue('categoria', $categoria['nombre']);
$templateProcessor->setValue('dias',  $r['dias']);
$templateProcessor->setValue('parMin',  $r['parMin']);
$templateProcessor->setValue('parMax', $r['parMax']);
$templateProcessor->setValue('edadMinima', $r['edadMinima']);
$templateProcessor->setValue('edadMaxima', $r['edadMaxima']);
$templateProcessor->setValue('fechaInscripcion', $r['fechaInscripciones']);
$templateProcessor->setValue('fechaInicio', $r['fechaInicio']);
$templateProcessor->setValue('fechaFin', $r['fechaFin']);
$templateProcessor->setValue('horaInicio', $r['horaInicio']);
$templateProcessor->setValue('horaFin', $r['horaFin']);
$templateProcessor->setValue('precio', $r['precio'].' €');
$templateProcessor->setValue('responsables', implode(",", $nombres));
$templateProcessor->setValue('ubicacion', $r['lugarCelebracion']);
$templateProcessor->setValue('incluye', $r['incluye']);
$templateProcessor->setValue('textoPrevio', $r['textoPrevioFolleto']);
$templateProcessor->setValue('observaciones', $r['observaciones']);
$templateProcessor->setValue('tiempo', $tiempo['HORAS']);

//echo date('H:i:s'), ' Guardando el documento...</br>';

$templateProcessor->saveAs('words/'.$r['nombre'].'.docx');

//Set header to show as PDF
header("Content-Type: application/pdf");
header("Content-Disposition: inline; filename=" .$r['nombre'].'.pdf');

//Create a temporary file for Word
//$temp = tmpfile();
//fwrite($temp, $data["data"]); //Write the data in the file
//$uri = stream_get_meta_data($temp)["uri"]; //Get the location of the temp file

//Convert the docx file in to an PhpWord Object
$doc = PhpOffice\PhpWord\IOFactory::load('words/'.$r['nombre'].'.docx');

//Set the PDF Engine Renderer Path. Many engines are supported (TCPDF, DOMPDF, ...).
\PhpOffice\PhpWord\Settings::setPdfRendererPath("vendor/tecnickcom/tcpdf");
\PhpOffice\PhpWord\Settings::setPdfRendererName('TCPDF');

//Create a writer, which converts the PhpWord Object into an PDF
$xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($doc, 'PDF');

//Create again an temp file for the new generated PDF.
$pdf_temp = tmpfile();
$pdf_uri = stream_get_meta_data($pdf_temp)["uri"];

//Save the PDF to the path
$xmlWriter->save($pdf_uri);

//Now print the file from the temp path.
echo file_get_contents($pdf_uri);

//echo file_get_contents('words/'.$r['nombre'].'.docx');

?>