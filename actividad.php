<?php
include  'conexion.php';

// Conexión
$cn    = ConexionMySql();

// Selección responsables
$sqlResponsable   = "SELECT * FROM responsables ORDER BY nombre ASC";
$queryResponsable = $cn->query( $sqlResponsable );

// Selección aulas
$sqlAula = "SELECT * FROM aulas ORDER BY nombre ASC";
$queryAula = $cn->query( $sqlAula );

// Selección gurpos
$sqlGrupo = "SELECT * FROM grupos ORDER BY nombre ASC";
$queryGrupo = $cn->query( $sqlGrupo );

// Selección categoias
$sqlCategoria   = "SELECT * FROM categorias ORDER BY nombre ASC";
$queryCategoria = $cn->query( $sqlCategoria );

// Selección de actividad por parámetro
$sql   = "SELECT * FROM actividades WHERE idActividad=:idActividad";
$query = $cn->prepare($sql);
$query->bindParam('idActividad', $_GET["id"]);
$query->execute();
$r = $query->fetch(PDO::FETCH_ASSOC);

// Selección responsables dependiendo de la actividad
$sqlActividadResponsable   = "SELECT * FROM actividadesResponsables WHERE idActividadResponsable = :actividadResponsableId";
$queryActividadResponsable = $cn->prepare($sqlActividadResponsable);
$queryActividadResponsable->bindParam(':actividadResponsableId', $r['actividadResponsableId']);
$queryActividadResponsable->execute();
//$t = $queryActividadResponsable->fetchAll(PDO::FETCH_ASSOC);

//
$sqlResponsable2   = "SELECT idResponsable FROM responsables WHERE idResponsable=:responsableId";
$queryResponsable2 = $cn->prepare($sqlResponsable2);

?>
  <!DOCTYPE html>
  <html lang="es">

  <head>
    <title>Ver actividad | Rozas Joven</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb"
      crossorigin="anonymous">
    <link rel="stylesheet" href="../../css/jquery-ui.min.css">
    <link rel="stylesheet" href="../../css/jquery-ui.structure.min.css">
    <!-- <link rel="stylesheet" href="css/bootstrap-datepicker.standalone.css"> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../css/bootstrap-datetimepicker.css">
    <link rel="stylesheet" href="../../css/jquery-ui.min.css">
  </head>

  <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
      <a class="navbar-brand" href="#">Actividades Rozas Joven</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"
        aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="./">Inicio
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="aula.php">Aulas</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="categoria.php">Categorias</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="grupo.php">Grupos</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="responsable.php">Responsables</a>
          </li>

          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
              aria-expanded="false">Nuevo</a>
            <div class="dropdown-menu mr-sm-2" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="acciones/insertar/insertarActividad.php">Actividad</a>
              <a class="dropdown-item" href="acciones/insertar/insertarAula.php">Aula</a>
              <a class="dropdown-item" href="acciones/insertar/insertarCategoria.php">Categoria</a>
              <a class="dropdown-item" href="acciones/insertar/insertarGrupo.php">Grupo</a>
              <a class="dropdown-item" href="acciones/insertar/insertarResponsable.php">Responsable</a>
            </div>
          </li>

        </ul>
      </div>
    </nav>
    <div class="container">
      <h1 class="text-center text-info">VER ACTIVIDAD</h1>
      <form  method="POST" action="word.php">
      <div class="form-group text-center col-12">
          <input type="hidden" name="idActividad" id="idActividad" name="idActividad" value="<?php echo $r['idActividad']?>">
          <button class="text-white btn btn-primary float-center" type="submit">Descargar</button>
        </div>
        <!-- PRIMERA FILA -->
        <div class="form-group">
          <label for="nombre">Actividad</label>
          <input disabled  type="text" class="form-control" value="<?php echo $r['nombre']?>">
        </div>
        <!-- SEGUNDA FILA -->
        <div class="form-row">
          <div class="col-md-3">
            <div class="form-group">
              <label for="grupoId">Grupo</label>
              <select disabled  class="form-control">
                <?php
              while( $s = $queryGrupo->fetch() ) {
            ?>
                  <option value="<?php echo intval($s[0]) ?>">
                    <?php echo $s[1]?>
                  </option>
                  <?php } ?>
              </select>
            </div>
            <div class="form-group">
              <label for="aulaId">Aula</label>
              <select disabled  class="form-control">
                <?php
              while( $s = $queryAula->fetch() ) {
            ?>
                  <option value="<?php echo intval($s[0]) ?>">
                    <?php echo $s[1]?>
                  </option>
                  <?php } ?>
              </select>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="actividadResponsableId">Responsable</label>
              <select disabled  multiple class="form-control">
                <?php
                  $encontrado = false;
                  $contador = 0;
                  $resultado = $queryActividadResponsable->fetchAll(PDO::FETCH_ASSOC);
                  while( $s = $queryResponsable->fetch(PDO::FETCH_ASSOC) ) {
                ?>
                  <option value="<?php echo intval($s['idResponsable']) ?>" 
                    <?php
                      for($i=0 ; $i<count($resultado); $i++){ 
                        if( intval($s[ 'idResponsable'])==intval($resultado[$i][ 'responsableId']) ){ 
                          $contador++;
                          $encontrado=true; 
                        }; 
                      }; 
                      if($encontrado){ 
                        echo ' selected';
                        $encontrado=false; 
                      }; 
                    ?>
                      >
                      <?php echo $s['nombre']?>
                    </option>
                    <?php } ?>
              </select>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="fechaInicio">Fecha de inicio</label>
              <br>
              <div class="input-group">
                <span class="input-group-addon">
                  <i class="fa fa-calendar" aria-hidden="true"></i>
                </span>
                <input disabled  class="fecha" type="text" value="<?php echo $r['fechaInicio']?>">
              </div>
            </div>
            <div class="form-group">
              <label for="fechaFin">Fecha de fin</label>
              <br>
              <div class="input-group">
                <span class="input-group-addon">
                  <i class="fa fa-calendar" aria-hidden="true"></i>
                </span>
                <input disabled  class="fecha" type="text" value="<?php echo $r['fechaFin']?>">
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="dias">Días</label>
              <br>
              <input disabled  type="text" value="<?php echo $r['dias']?>">
            </div>
            <div class="form-group">
              <label for="precio">Precio</label>
              <br>
              <input disabled type="number" step="1" min=0 value="<?php echo $r['precio']?>">
            </div>
          </div>
        </div>
        <!-- TERCERA FILA -->
        <div class="form-row">
          <div class="col-md-3">
            <div class="form-group">
              <label for="horaInicio">Hora de inicio</label>
              <br>
              <div class="input-group">
                <span class="input-group-addon">
                  <i class="fa fa-clock-o" aria-hidden="true"></i>
                </span>
                <input disabled class="hora" type="text" value="<?php echo $r['horaInicio']?>">
              </div>
            </div>
            <div class="form-group">
              <label for="horaFin">Hora de fin</label>
              <br>
              <div class="input-group">
                <span class="input-group-addon">
                  <i class="fa fa-clock-o" aria-hidden="true"></i>
                </span>
                <input disabled class="hora" type="text" value="<?php echo $r['horaFin']?>">
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="parMin">Par.Min.</label>
              <br>
              <div class="input-group">
                <span class="input-group-addon">
                  <i class="fa fa-users" aria-hidden="true"></i>
                </span>
                <input disabled type="number" value="<?php echo $r['parMin']?>">
              </div>
            </div>
            <div class="form-group">
              <label for="parMax">Par.Max.</label>
              <br>
              <div class="input-group">
                <span class="input-group-addon">
                  <i class="fa fa-users" aria-hidden="true"></i>
                </span>
                <input disabled type="number" value="<?php echo $r['parMax']?>">
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="edadMinima">Edad mínima</label>
              <br>
              <div class="input-group">
                <span class="input-group-addon">
                  <i class="fa fa-birthday-cake" aria-hidden="true"></i>
                </span>
                <input disabled type="number" value="<?php echo $r['edadMinima']?>">
              </div>
            </div>
            <div class="form-group">
              <label for="edadMaxima">Edad máxima</label>
              <br>
              <div class="input-group">
                <span class="input-group-addon">
                  <i class="fa fa-birthday-cake" aria-hidden="true"></i>
                </span>
                <input disabled type="number" value="<?php echo $r['edadMaxima']?>">
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="fechaInscripciones">Fecha de inscripciones</label>
              <br>
              <div class="input-group">
                <span class="input-group-addon">
                  <i class="fa fa-calendar" aria-hidden="true"></i>
                </span>
                <input disabled class="fecha" type="text" value="<?php echo $r['fechaInscripciones']?>"
                />
              </div>
            </div>
            <div class="form-group">
              <label for="categoriaId">Categoria</label>
              <select disabled class="form-control">
                <?php
              while( $s = $queryCategoria->fetch() ) {
            ?>
                  <option value="<?php echo intval($s[0]) ?>"
                  <?php
                    if(intval($s[0]) == $r['categoriaId'])
                      echo 'selected';
                  ?>
                  >
                    <?php echo $s[1]?>
                  </option>
                  <?php } ?>
              </select>
            </div>
          </div>
        </div>
        <!-- CUARTA FILA -->
        <div class="form-row">
          <div class="form-group col-md-12">
            <label for="incluye">Incluye</label>
            <textarea readonly class="form-control" rows="2"><?php echo$r['incluye'];?>
            </textarea>
          </div>
          <div class="form-group col-md-12">
            <label for="textoPrevioFolleto">Texto previo folleto</label>
            <textarea readonly class="form-control" rows="2"><?php echo $r['textoPrevioFolleto'];?>
            </textarea>
          </div>
          <div class="form-group col-md-12">
            <label for="lugarCelebracion">Lugar de la celebración</label>
            <input readonly type="text" class="form-control" value="<?php echo $r['lugarCelebracion'];?>"
            />
          </div>
          <div class="form-group col-md-12">
            <label for="observaciones">Observaciones</label>
            <textarea readonly class="form-control" rows="2"><?php echo $r['observaciones'] ?></textarea>
          </div>
        </div>
      </form>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
      crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh"
      crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ"
      crossorigin="anonymous"></script>
    <!-- <script src="js/bootstrap-datepicker.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.3/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.3/locale/es.js"></script>
    <script src="../../js/bootstrap-datetimepicker.js"></script>
    <script src="../../js/tiempo.js"></script>
  </body>