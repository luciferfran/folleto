<?php

function ConexionMySql()
{
    // $cadenaHost = 'mysql:Server=localhost;dbname=basebyjefferson';
    // cambiar por:
    $cadenaHost     = 'mysql:host=localhost;dbname=folleto';
    $nombre_user_db = 'root';
    $pass_user_db   = 'gilipoyas';
    try {
        $conectar = new PDO( $cadenaHost, $nombre_user_db, $pass_user_db );
        $conectar->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
    }
    catch( Exception $e ) {
        echo "ERROR: " . $e->getMessage();
    }

    return $conectar;
}